import React from 'react';
import AppBar from 'material-ui/AppBar';
import { browserHistory } from 'react-router'
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import IconButton from 'material-ui/IconButton';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {tilesData} from './GridMenu';


export default class Detail extends React.Component {
  static propTypes = {
    name: React.PropTypes.string,
  };

  constructor(props) {
    super(props);
  }
  _back(){
  	browserHistory.goBack();

  }
  _findData(name){
  	return tilesData.find((data) => {
  		return data.title == name;
  	});
  }
  _display(){
  	let name = this.props.params.name;
  	let hasil = this._findData(name);
  	
  	return <Card>
		    <CardHeader
		      title={hasil.title}
		      subtitle={"by "+hasil.author}
		    />
		    <CardMedia
		      overlay={<CardTitle title={hasil.title} subtitle={"by "+hasil.author} />}
		    >
		      <img src={hasil.img} />
		    </CardMedia>
		    <CardTitle title={hasil.title} subtitle={"by "+hasil.author} />
		    <CardText>
		      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		      Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
		      Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
		      Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
		    </CardText>
		  </Card>
  }
  render() {
    return (
      <div>
      	<AppBar 
          title="Detail"        
          onLeftIconButtonTouchTap = {this._back}
          iconElementLeft = { <IconButton><ArrowBack/></IconButton> }
        />
        { this._display() }
      </div>
    );
  }
}

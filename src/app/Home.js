import React,{ Component } from 'react';
import AppBar from 'material-ui/AppBar';
import LeftDrawer from './LeftDrawer';
import GridMenu from './GridMenu';


export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
    	leftDrawer : false
    }
  }
  handleLeftDrawer(){
  	this.setState({leftDrawer : !this.state.leftDrawer});
  }
  onChangeDrawer(leftDrawer){
  	this.setState({leftDrawer});
  }
  render() {
    return (
      <div>
      	<AppBar 
          title="React MUI"
          // iconClassNameRight="muidocs-icon-navigation-expand-more"          
          onLeftIconButtonTouchTap = {this.handleLeftDrawer.bind(this)}
        />
       	<LeftDrawer
       		open = { this.state.leftDrawer }
       		onChange = { this.onChangeDrawer.bind(this) }
       	/>
       	<GridMenu/>
      </div>
    );
  }
}

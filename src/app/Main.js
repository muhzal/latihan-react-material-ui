/**
 * In this file, we create a React component
 * which incorporates components provided by Material-UI.
 */
import React, {Component} from 'react';
import {deepOrange500} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Router, Route, hashHistory } from 'react-router'


import Home from './Home';
import Detail from './Detail';




const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500,
  },
});

class Main extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      leftBar: false,
    };
  }

  render() {
    
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <Router history={hashHistory}>
          <Route path="/" component={Home}/>
          <Route path="/detail" component={Detail}/>
          <Route path="/detail/:name" component={Detail}/>
        </Router>

      </MuiThemeProvider>
    );
  }
}

export default Main;

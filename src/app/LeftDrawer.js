import React from 'react';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Drawer from 'material-ui/Drawer';

import RemoveRedEye from 'material-ui/svg-icons/image/remove-red-eye';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import ContentLink from 'material-ui/svg-icons/content/link';
import Divider from 'material-ui/Divider';
import ContentCopy from 'material-ui/svg-icons/content/content-copy';
import Download from 'material-ui/svg-icons/file/file-download';
import Delete from 'material-ui/svg-icons/action/delete';
import FontIcon from 'material-ui/FontIcon';
import { Link } from 'react-router';
export default class LeftDrawer extends React.Component {
  
  constructor(props) {
    super(props);
  }

  render() {
    return (
       <Drawer
        	open={this.props.open}
			docked={false}
			onRequestChange={this.props.onChange}
        >        
	        <Menu>
		        <MenuItem primaryText="Detail" leftIcon={<RemoveRedEye />} containerElement={<Link to="/detail" />}/>
		        <MenuItem primaryText="Share" leftIcon={<PersonAdd />} />
		        <MenuItem primaryText="Get links" leftIcon={<ContentLink />} />
		        <Divider />
		        <MenuItem primaryText="Make a copy" leftIcon={<ContentCopy />} />
		        <MenuItem primaryText="Download" leftIcon={<Download />} />
		        <Divider />
		        <MenuItem primaryText="Remove" leftIcon={<Delete />} />
		     </Menu>  
        </Drawer>
    );
  }
}
